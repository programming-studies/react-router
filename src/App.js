/* eslint-disable */
import React from 'react';
import Router from './lib/Router/Router';
import Menu from './components/Menu';
import Content from './components/Content';
import './App.css';

function App() {
  return (
    <Router>
      <div className='App'>
        <Menu />
        <Content />
      </div>
    </Router>
  );
}

export default App;
