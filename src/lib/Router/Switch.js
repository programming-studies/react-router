import React from 'react';
import { RouterConsumer } from './RouterContext';

const Switch = ({ children } ) => {
  return (
    <RouterConsumer>
      {
        () => {
          return children
        }
      }
    </RouterConsumer>
  );
};

export default Switch;
