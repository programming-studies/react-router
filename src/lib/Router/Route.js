import { useContext } from 'react';
import { pathToRegexp } from 'path-to-regexp';
import RouterContext from './RouterContext';

const Route = ({ path, children }) => {
  const { state: { url } } = useContext(RouterContext);
  const re = pathToRegexp(path);

  if (!re.test(url)) return null;

  return children;
};

export default Route;

