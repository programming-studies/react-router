import React from 'react';
import Router from '../Router';
import Link from '../Link';
import { render } from '@testing-library/react';

describe('Link', () => {
  it('should render children', () => {
    const { getByText } = render(
      <Router>
        <Link path="/the-force">
          The Force
        </Link>
      </Router>
    );

    expect(getByText('The Force')).toBeInTheDocument();
  });

  it('should render the ‘a’ tag', () => {
    const { container } = render(
      <Router>
        <Link path="/the-force">
          The Force
        </Link>
      </Router>
    );

    expect(container.querySelector('a')).toBeInTheDocument();
  });

  it('should render the ‘a’ tag with proper ‘href’ attribute', () => {
    const { container } = render(
      <Router>
        <Link path="/the-force">
          The Force
        </Link>
      </Router>
    );

    const aTag = container.querySelector('a');
    expect(aTag.getAttribute('href')).toEqual('/the-force');
  });
});
