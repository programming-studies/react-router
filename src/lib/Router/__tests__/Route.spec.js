//
// References:
//
// https://www.ryandoll.com/post/2018/3/29/jest-and-url-mocking
//
// https://developer.mozilla.org/en-US/docs/Web/API/History/pushState
//

import React from 'react';
import { render } from '@testing-library/react';
import Router from '../Router';
import Route from '../Route';

describe('Route', () => {
  it('should fucking work', () => {
    const { getByText } = render(
      <Router>
        <Route path="/">
          <h2>Home</h2>
        </Route>
      </Router>
    );

    expect(getByText('Home')).toBeInTheDocument();
  });

  it('should not render when path does not match patname url', () => {
    window.history.pushState({}, 'About Page', '/about');

    const { container } = render(
      <Router>
        <Route path="/welcome">
          <h2>Welcome</h2>
        </Route>
      </Router>
    );

    expect(container.firstElementChild).toBeNull();
  });

  it('should render the given path component', () => {
    window.history.pushState({}, 'Hello Page', '/hello');

    const { getByText } = render(
      <Router>
        <Route path="/hello">
          <h2>Hello</h2>
        </Route>
      </Router>
    );

    expect(window.location.pathname).toEqual('/hello');
    expect(getByText('Hello')).toBeInTheDocument();
  });
});

