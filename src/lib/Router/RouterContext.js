import { createContext } from 'react';

const RouterContext = createContext();
const { Provider, Consumer } = RouterContext;

export {
  RouterContext as default,
  Provider as RouterProvider,
  Consumer as RouterConsumer,
};
