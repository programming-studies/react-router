import React, { useContext } from 'react';
import RouterContext from './RouterContext';

const Link = function Link ({ path, children }) {
  const { go } = useContext(RouterContext);

  return (
    <a
      href={path}
      onClick={(evt) => {
        evt.preventDefault();
        go(path);
      }}
    >
      {children}
    </a>
  );
};

export default Link;
