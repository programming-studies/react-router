import React, {
  useState,
  useEffect,
} from 'react';

import history from 'browser-history';

import { RouterProvider } from './RouterContext';

const Router = function Router ({ children, ...props }) {
  const [url, setUrl] = useState(window.location.pathname);

  //
  // When the component mounts (therefore the empty ‘[]’), we want to set the
  // browser history to the current url, since that is the first item in the
  // history.
  //
  useEffect(() => {
    history((e, url) => {
      setUrl(url);
    });
  }, []);

  const go = function go (url) {
    setUrl(url);

    // And when navigating to a new url, add it to the history too.
    history(url);
  };

  return (
    <RouterProvider
      value={{
        state: { url },
        go,
      }}
    >
      {children}
    </RouterProvider>
  );
};

export {
  Router as default,
};
