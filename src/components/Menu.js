import React from 'react';
import Link from '../lib/Router/Link';

import styles from './Menu.module.css';

const Menu = () => {
  return (
    <nav className={styles.nav}>
      <ul>
        <li>
          <Link path='/'>Home</Link>
        </li>
        <li>
          <Link path='/about'>About</Link>
        </li>
        <li>
          <Link path='/projects'>Projects</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Menu;
