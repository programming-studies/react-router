import React from 'react';
import Switch from '../lib/Router/Switch';
import Route from '../lib/Router/Route';
import PageHome from '../pages/PageHome';
import PageAbout from '../pages/PageAbout';
import PageProjects from '../pages/PageProjects';

import styles from './Content.module.css';

const Content = () => {
  return (
    <section className={styles.content}>
      <Switch>
        <Route path='/'><PageHome /></Route>
        <Route path='/about'><PageAbout /></Route>
        <Route path='/projects'><PageProjects /></Route>
      </Switch>
    </section>
  );
};

export default Content;
